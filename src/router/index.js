import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Games from '../views/Games.vue'
import GameDetail from '../views/GameDetail.vue'
import Cart from '../views/Cart.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/games',
        name: 'Games',
        component: Games
    },
    {
        path: '/games/:id',
        name: 'Game Detail',
        component: GameDetail
    },
    {
        path: '/cart',
        name: 'Cart',
        component: Cart
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router